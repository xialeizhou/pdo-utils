<?php
/**
* @file WenTou.php
* @brief simple reading data from the database
* @author xialeizhou
* @version 0.1.0
* @date 2014-11-20
 */

if (!interface_exists('JsonSerializable')) {
    interface JsonSerializable {
        function jsonSerialize();
    }
}

include_once dirname(__FILE__) . "/WenTou/Structure.php";
include_once dirname(__FILE__) . "/WenTou/Cache.php";
include_once dirname(__FILE__) . "/WenTou/Literal.php";
include_once dirname(__FILE__) . "/WenTou/Result.php";
include_once dirname(__FILE__) . "/WenTou/MultiResult.php";
include_once dirname(__FILE__) . "/WenTou/Row.php";

// friend visibility emulation
abstract class WenTouAbstract {
    protected $connection, $driver, $structure, $cache;
    protected $wenTou, $table, $primary, $rows, $referenced = array();

    protected $debug = false;
    protected $debugTimer;
    protected $freeze = false;
    protected $rowClass = 'WenTouRow';
    protected $jsonAsArray = false;

    protected function access($key, $delete = false) {
    }

}

/** Database representation
 * @property-write mixed $debug = false Enable debugging queries, true for error_log($query), callback($query, $parameters) otherwise
 * @property-write bool $freeze = false Disable persistence
 * @property-write string $rowClass = 'WenTouRow' Class used for created objects
 * @property-write bool $jsonAsArray = false Use array instead of object in Result JSON serialization
 * @property-write string $transaction Assign 'BEGIN', 'COMMIT' or 'ROLLBACK' to start or stop transaction
 */
class WenTou extends WenTouAbstract {

    /** Create database representation
     * @param PDO
     * @param WenTou_Structure or null for new WenTou_Structure_Convention
     * @param WenTou_Cache or null for no cache
     */
    function __construct(PDO $connection, WenTou_Structure $structure = null, WenTou_Cache $cache = null) {
        $this->connection = $connection;
        $this->driver = $connection->getAttribute(PDO::ATTR_DRIVER_NAME);
        if (!isset($structure)) {
            $structure = new WenTou_Structure_Convention;
        }
        $this->structure = $structure;
        $this->cache = $cache;
    }

    /** Get table data to use as $db->table[1]
     * @param string
     * @return WenTouResult
     */
    function __get($table) {
        return new WenTouResult($this->structure->getReferencingTable($table, ''), $this, true);
    }

    /** Set write-only properties
     * @return null
     */
    function __set($name, $value) {
        if ($name == "debug" || $name == "debugTimer" || $name == "freeze" || $name == "rowClass" || $name == "jsonAsArray") {
            $this->$name = $value;
        }
        if ($name == "transaction") {
            switch (strtoupper($value)) {
            case "BEGIN": return $this->connection->beginTransaction();
            case "COMMIT": return $this->connection->commit();
            case "ROLLBACK": return $this->connection->rollback();
            }
        }
    }

    /** Get table data
     * @param string
     * @param array (["condition"[, array("value")]]) passed to WenTouResult::where()
     * @return WenTouResult
     */
    function __call($table, array $where) {
        $return = new WenTouResult($this->structure->getReferencingTable($table, ''), $this);
        if ($where) {
            call_user_func_array(array($return, 'where'), $where);
        }
        return $return;
    }

}
