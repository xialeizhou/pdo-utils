pdo-utils
=========
#1.说明
pdo-utils是 一个基于PDO的数据库交互封装层，方便用户构建多数据库交互的web service
#2.使用
##2.1. 和mysql交互
### 配置
修改config/ws.connect.mysql.php :
``` php
$host ='your ip';
$port = 'port';
$dbname = 'database name';
$user = 'user';
$password = 'password';

$connection = new PDO(
    "mysql:host=$host;port=$port;dbname=$dbname",
    "$user",
    "$password"
);
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
$connection->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
$mysql = new WenTou($connection);
```
### 使用

```php
require_once('./config/ws.config.mysql.php')
$table = $mysql->onedb()->order('id')->fetchPairs("id", "title");
```
##2.2. 和pgsql交互
###配置
修改config/ws.connect.pgsql.php :

```php
... 同上
$connection = new PDO(
    "pgsql:host=$host;port=$port;dbname=$dbname",
    "$user",
    "$password"
);
... 同上
```
### 使用
```php
require_once('./config/ws.config.mysql.php')
...同上
```
#3.demo
## 3.1 Connecting

```php
include "WenTou.php";
$pdo = new PDO("mysql:dbname=software");
$db = new WenTou($pdo);
```
WenTou uses PDO driver to access the database.

##3.2 Reading data

```php
foreach ($db->application() as $application) { // get all applications
        echo "$application[title]\n"; // print application title
}
```
Use a method call to get rows from the table. Use square brackets to access a column value.

## 3.3 Limiting result set

```php
$applications = $db->application()
        ->select("id, title")
        ->where("web LIKE ?", "http://%")
        ->order("title")
        ->limit(10)
;
foreach ($applications as $id => $application) {
        echo "$application[title]\n";
}
```

The example gets first ten applications with HTTP URL ordered by a title.

### 3.3 Getting a single row

```php
$application = $db->application[1]; // get by primary key
$application = $db->application("title = ?", "Adminer")->fetch();
```

## 3.4 Relationships

```php
echo $application->author["name"] . "\n"; // get name of the application author
foreach ($application->application_tag() as $application_tag) { // get all tags of $application
        echo $application_tag->tag["name"] . "\n"; // print the tag name
}
```

Simple work with relationships is a killer feature of WenTou. Method call ->() on a row object gets all rows from referencing table, member access -> stands for table referenced by the current row, and array access [] gets always a value in the row. Checkout the performance of this code.

## 3.5 Joins

```php
// get all applications ordered by author's name
foreach ($db->application()->order("author.name") as $application) {
        echo $application->author["name"] . ": $application[title]\n";
}
```

If you need to filter or order rows by column in referenced table then simply use the dot notation.

## 3.6 Grouping

```php
echo $db->application()->max("id"); // get maximum ID
foreach ($db->application() as $application) {
        // get count of each application's tags
        echo $application->application_tag()->count("*") . "\n";
}
```

All grouping functions are supported.

#4.API
## Database

| api                                               | comment
|:-------------------------------------------------:|:------------------------------------------------------------------------------------------------
| $db = new WenTou($pdo[, $structure[, $cache]])    | Get database representation
| $db->debug = true                                 | Call error_log("$file:$line:$query; -- $parameters") before executing a query
| $db->debug = $callback                            | Call $callback($query, $parameters) before executing a query, if $callback would return false then do not execute the query
| $db->debugTimer = $callback                       | Call $callback() after executing a query
| $db->rowClass = 'ClassName'                       | Derive row objects from this class (defaults to 'WenTou_Row')
| $db->transaction = $command                       | Assign 'BEGIN', 'COMMIT' or 'ROLLBACK' to start or stop transaction

## Result

| api                                                       | comment
|:---------------------------------------------------------:|:------------------------------------------------------------------
| $table = $db->$tableName([$where[, $parameters[, ...]]])  | representation of table $tableName
| $table->where($where[, $parameters[, ...]])               | Set WHERE (explained later)
| $table->and($where[, $parameters[, ...]])                 | Add AND condition
| $table->or($where[, $parameters[, ...]])                  | Add OR condition
| $table->order($columns[, ...])                            | Set ORDER BY, can be expression ("column DESC, id DESC")
| $table->order("")                                         | Reset previously set order
| $table->select($columns[, ...])                           | Set retrieved columns, can be expression ("col, MD5(col) AS hash")
| $table->select("")                                        | Reset previously set select columns
| $table->limit($limit[, $offset])                          | Set LIMIT and OFFSET
| $table->group($columns[, $having])                        | Set GROUP BY and HAVING
| $table->union($table2[, $all])                            | Create UNION
| $table->lock($exclusive)                                  | Append FOR UPDATE (default) or LOCK IN SHARE MODE (non-exclusive)
| $array = $table->fetchPairs($key, $value)                 | Fetch all values to associative array
| $array = $table->fetchPairs($key)                         | Fetch all rows to associative array
| count($table)                                             | Get number of rows in result set
| (string) $table                                           | Get SQL query
| $table($where[, $parameters[, ...]])                      | Shortcut for $table->where() since PHP 5.3

## Class diagram
Repetitive calls of where, order and select are possible – it will append values to the previous calls (where by AND). These methods plus group and limit provide a fluent interface so $table->where()->order()->limit() is possible (in any order). All methods are lazy – the query is not executed until necessary.

The $where parameter can contain ? or :name which is bound by PDO to $parameters (so no manual escaping is required). The $parameters[, ...] can be one array, one associative array or zero or more scalars. If the question mark and colon are missing but $parameters are still passed then the behavior is this:

| api                                                                 | comment
|:-------------------------------------------------------------------:|:---------------------------------------------------------------------------
| $table->where("field", "x")                                         | Translated to field = 'x' (with automatic escaping)
| $table->where("field", null)                                        | Translated to field IS NULL
| $table->where("field", array("x", "y"))                             | Translated to field IN ('x', 'y') (with automatic escaping)
| $table->where("NOT field", array("x", "y"))                         | Translated to NOT field IN ('x', 'y') (with automatic escaping)
| $table->where("field", $db->$tableName())                           | Translated to "field IN (SELECT $primary FROM $tableName)"
| $table->where("field", $db->$tableName()->select($column))          | Translated to "field IN (SELECT $column FROM $tableName)"
| $table->where("field > ? AND field < ?", "x", "y")                  | Bound by PDO
| $table->where("(field1, field2)", array(array(1, 2), array(3, 4)))  | Translated to (field1, field2) IN ((1, 2), (3, 4)) (with automatic escaping)
$table->where(array("field" => "x", "field2" => "y"))               | Translated to field = 'x' AND field2 = 'y' (with automatic escaping)

If the dot notation is used for a column anywhere in the query ("$table.$column") then WenTou automatically creates left join to the referenced table. Even references across several tables are possible ("$table1.$table2.$column"). Referencing tables can be accessed by colon: $applications->select("COUNT(application_tag:tag_id)").

The result also supports aggregation:

| api                                             | comment
|:-----------------------------------------------:|:--------------------------
| $table->count("*")                              | Get number of rows
| $table->count("DISTINCT $column")               | Get number of distinct values
| $table->sum($column)                            | Get minimum value
| $table->min($column)                            | Get minimum value
| $table->max($column)                            | Get maximum value
| $table->aggregation("GROUP_CONCAT($column)")    | Run any aggregation function

##Row

| api                                                         | comment
|:-----------------------------------------------------------:|:-----------------------------------------------------------------------------------------------------------
| foreach ($table as $id => $row)                             | Iterate all rows in result
| $row = $db->{$tableName}[$id]                               | Get single row with ID $id from table $tableName (null if the row does not exist)
| $row = $table[$id]                                          | Get row with primary key $id from the result
| $row = $table[array($column => $value)]                     | Get first row with $column equals to $value
| $row = $table->fetch()                                      | Get next row from the result
| $data = $row[$column]                                       | Get data from column $column
| $row2 = $row->$tableName                                    | Get row from referenced table
| $table2 = $row->$tableName([$where[, $parameters[, ...]]])  | Get result from referencing table
| $table2 = $row->$tableName()->via($column)                  | Use non-default column (e.g. created_by)
| foreach ($row as $column => $data)                          | Iterate all values in row
| count($row)                                                 | Get number of columns in row
| (string) $row                                               | Get primary key value
| $row["author_id"]                                           | gets the numerical value of column, $row->author gets row representing the author from the table of authors.

$row->$tableName() builds "SELECT * FROM $tableName WHERE {$myTable}_id = $row[$primary]". The returned object can be customized as usual table results (with order, limit, ...) and then executed. All results are lazy and execute only a single query for each set of rows (the result is stored to memory so following modifications will not affect it).

##Structure

The object passed in second argument of WenTou constructor must implement the WenTou_Structure interface:

| api                                     | comment
|:---------------------------------------:|:---------------------------------------------------
| getPrimary($table)                      | Get primary key of a table in $db->$table()
| getReferencingColumn($name, $table)     | Get column holding foreign key in $table[$id]->$name()
| getReferencingTable($name, $table)      | Get target table in $table[$id]->$name()
| getReferencedColumn($name, $table)      | Get column holding foreign key in $table[$id]->$name
| getReferencedTable($name, $table)       | Get table holding foreign key in $table[$id]->$name
| getSequence($table)                     | Get sequence name (after insert)

WenTou defines a structure suitable for databases with some convention (this is the default structure):
```php
$structure = new WenTou_Structure_Convention(
        $primary = 'id',
        $foreign = '%s_id',
        $table = '%s',
        $prefix = ''
);
```
There is also an auto-discovery class for MySQL. It reads meta-information from the database, which slows things down (but it can be cached).

```php`
$structure = new WenTou_Structure_Discovery($pdo, $cache = null, $foreign = '%s');
```
See more about structure.

##Cache

The object passed in third argument of WenTou constructor must implement the WenTou_Cache interface (default null stands for no cache).

api                   | comment
----------------------|----------------------------------------------------------------
load($key)            | Get data stored in the cache (return null if there is no data)
save($key, $data)     | Save data to cache

WenTou defines following caches:

| api                                           | comment
|:---------------------------------------------:|:--------------------------------------------
| new WenTou_Cache_Session                      | Uses $_SESSION["WenTou"]
| new WenTou_Cache_File($filename)              | Serializes to $filename
| new WenTou_Cache_Include($filename)           | Includes $filename (very fast with PHP accelerators)
| new WenTou_Cache_Database(PDO $connection)    | Uses notorm table in the database
| new WenTou_Cache_Memcache(Memcache $memcache) | Uses WenTou. prefix in Memcache
| new WenTou_Cache_APC                          | Uses WenTou. prefix in APC

The table used by WenTou_Cache_Database must already exist (script should not have privileges to create it anyway):

```php
CREATE TABLE notorm (
        id varchar(255) NOT NULL,
        data text NOT NULL,
        PRIMARY KEY (id)
)
```

##Error reporting

WenTou does not escalate errors. Non-existing table produces SQL error that is reported by PDO conforming to PDO::ATTR_ERRMODE. Non-existing columns produces the same E_NOTICE as an attempt to access non-existing key in array.
