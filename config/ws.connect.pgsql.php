<?php
error_reporting(E_ALL | E_STRICT);
include dirname(__FILE__) . "/WenTou.php";

$host ='your ip';
$port = 'port';
$dbname = 'database name';
$user = 'user';
$password = 'password';

$connection = new PDO(
    "mysql:host=$host;port=$port;dbname=$dbname",
    "$user",
    "$password"
);
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
$connection->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
$pgsql = new WenTou($connection);
