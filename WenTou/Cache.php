<?php

/** Loading and saving data, it's only cache so load() does not need to block until save()
 */
interface WenTou_Cache {

    /** Load stored data
     * @param string
     * @return mixed or null if not found
     */
    function load($key);

    /** Save data
     * @param string
     * @param mixed
     * @return null
     */
    function save($key, $data);

}



/** Cache using $_SESSION["WenTou"]
 */
class WenTou_Cache_Session implements WenTou_Cache {

    function load($key) {
        if (!isset($_SESSION["WenTou"][$key])) {
            return null;
        }
        return $_SESSION["WenTou"][$key];
    }

    function save($key, $data) {
        $_SESSION["WenTou"][$key] = $data;
    }

}



/** Cache using file
 */
class WenTou_Cache_File implements WenTou_Cache {
    private $filename, $data = array();

    function __construct($filename) {
        $this->filename = $filename;
        $this->data = unserialize(@file_get_contents($filename)); // @ - file may not exist
    }

    function load($key) {
        if (!isset($this->data[$key])) {
            return null;
        }
        return $this->data[$key];
    }

    function save($key, $data) {
        if (!isset($this->data[$key]) || $this->data[$key] !== $data) {
            $this->data[$key] = $data;
            file_put_contents($this->filename, serialize($this->data), LOCK_EX);
        }
    }

}



/** Cache using PHP include
 */
class WenTou_Cache_Include implements WenTou_Cache {
    private $filename, $data = array();

    function __construct($filename) {
        $this->filename = $filename;
        $this->data = @include realpath($filename); // @ - file may not exist, realpath() to not include from include_path //! silently falls with syntax error and fails with unreadable file
        if (!is_array($this->data)) { // empty file returns 1
            $this->data = array();
        }
    }

    function load($key) {
        if (!isset($this->data[$key])) {
            return null;
        }
        return $this->data[$key];
    }

    function save($key, $data) {
        if (!isset($this->data[$key]) || $this->data[$key] !== $data) {
            $this->data[$key] = $data;
            file_put_contents($this->filename, '<?php return ' . var_export($this->data, true) . ';', LOCK_EX);
        }
    }

}



/** Cache storing data to the "notorm" table in database
 */
class WenTou_Cache_Database implements WenTou_Cache {
    private $connection;

    function __construct(PDO $connection) {
        $this->connection = $connection;
    }

    function load($key) {
        $result = $this->connection->prepare("SELECT data FROM notorm WHERE id = ?");
        $result->execute(array($key));
        $return = $result->fetchColumn();
        if (!$return) {
            return null;
        }
        return unserialize($return);
    }

    function save($key, $data) {
        // REPLACE is not supported by PostgreSQL and MS SQL
        $parameters = array(serialize($data), $key);
        $result = $this->connection->prepare("UPDATE notorm SET data = ? WHERE id = ?");
        $result->execute($parameters);
        if (!$result->rowCount()) {
            $result = $this->connection->prepare("INSERT INTO notorm (data, id) VALUES (?, ?)");
            try {
                @$result->execute($parameters); // @ - ignore duplicate key error
            } catch (PDOException $e) {
                if ($e->getCode() != "23000") { // "23000" - duplicate key
                    throw $e;
                }
            }
        }
    }

}



// eAccelerator - user cache is obsoleted



/** Cache using "WenTou." prefix in Memcache
 */
class WenTou_Cache_Memcache implements WenTou_Cache {
    private $memcache;

    function __construct(Memcache $memcache) {
        $this->memcache = $memcache;
    }

    function load($key) {
        $return = $this->memcache->get("WenTou.$key");
        if ($return === false) {
            return null;
        }
        return $return;
    }

    function save($key, $data) {
        $this->memcache->set("WenTou.$key", $data);
    }

}



/** Cache using "WenTou." prefix in APC
 */
class WenTou_Cache_APC implements WenTou_Cache {

    function load($key) {
        $return = apc_fetch("WenTou.$key", $success);
        if (!$success) {
            return null;
        }
        return $return;
    }

    function save($key, $data) {
        apc_store("WenTou.$key", $data);
    }

}
